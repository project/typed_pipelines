<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines;

use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\typed_pipelines\Mapping\Property;
use Drupal\typed_pipelines\Mapping\PropertyDefinition;
use Drupal\typed_pipelines\TypedData\DataDefinitionSerializer;

/**
 * A factory for creating pipelines from mappings.
 */
final class PipelineFactory {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  private TypedDataManagerInterface $typedDataManager;

  /**
   * The manipulator manager.
   *
   * @var \Drupal\typed_pipelines\ManipulatorManager
   */
  private ManipulatorManager $manipulatorManager;

  /**
   * The data definition serializer.
   *
   * @var \Drupal\typed_pipelines\TypedData\DataDefinitionSerializer
   */
  private DataDefinitionSerializer $dataDefinitionSerializer;

  /**
   * Constructs a new PipelineFactory object.
   *
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typedDataManager
   *   The typed data manager.
   * @param \Drupal\typed_pipelines\ManipulatorManager $manipulatorManager
   *   The manipulator manager.
   * @param \Drupal\typed_pipelines\TypedData\DataDefinitionSerializer $dataDefinitionSerializer
   *   The data definition serializer.
   */
  public function __construct(TypedDataManagerInterface $typedDataManager, ManipulatorManager $manipulatorManager, DataDefinitionSerializer $dataDefinitionSerializer) {
    $this->typedDataManager = $typedDataManager;
    $this->manipulatorManager = $manipulatorManager;
    $this->dataDefinitionSerializer = $dataDefinitionSerializer;
  }

  /**
   * Creates a pipeline from a mapping.
   *
   * @param array $mapping
   *   The mapping.
   */
  public function create(array $mapping): Pipeline {
    /** @var array<int, array<string, mixed>> $mapping */
    $properties_mapping = array_values(array_map(function (array $item) {
      /** @var array<string, array> $item */
      return new Property(
        $this->getPropertyDefinition($item['source']),
        $this->getPropertyDefinition($item['destination']),
        $this->getPropertyManipulators($item['manipulators'] ?? []),
      );
    }, $mapping));

    return new Pipeline(
      $this->typedDataManager,
      $properties_mapping
    );
  }

  /**
   * Get the property manipulations.
   *
   * @param array $configurations
   *   The configurations for the manipulators.
   *
   * @return \Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface[]
   *   The manipulator instances.
   */
  private function getPropertyManipulators(array $configurations): array {
    return array_values(array_map(function (array $item) {
      assert(is_string($item['id']));
      assert(is_array($item['configuration']));
      return $this->manipulatorManager->createInstance($item['id'], $item['configuration']);
    }, $configurations));
  }

  /**
   * Get a property definition from an array.
   *
   * @param array $data
   *   The definition data.
   */
  private function getPropertyDefinition(array $data): PropertyDefinition {
    assert(is_string($data['path']));
    assert(is_array($data['definition']));
    $property_data_definition = $this->dataDefinitionSerializer->deserialize($data['definition']);
    return new PropertyDefinition($data['path'], $property_data_definition);
  }

}
