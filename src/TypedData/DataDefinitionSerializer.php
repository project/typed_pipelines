<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\TypedData;

use Drupal\Core\TypedData\ComplexDataDefinitionInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\ListDataDefinitionInterface;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\Core\TypedData\TypedDataManagerInterface;

/**
 * Serializer for data definitions.
 */
final class DataDefinitionSerializer {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  private TypedDataManagerInterface $typedDataManager;

  /**
   * Constructs a new DataDefinitionSerializer object.
   *
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typedDataManager
   *   The typed data manager.
   */
  public function __construct(TypedDataManagerInterface $typedDataManager) {
    $this->typedDataManager = $typedDataManager;
  }

  /**
   * Serializes a data definition to an array.
   *
   * @return array
   *   The data definition, a data definition object.
   */
  public function serialize(DataDefinitionInterface $definition): array {
    $data = [];
    if (method_exists($definition, 'toArray')) {
      $data = $definition->toArray();
    }
    if ($definition instanceof ListDataDefinitionInterface) {
      $data['type'] = 'list';
      $item_type = $definition->getItemDefinition();
      $data['item_type'] = $this->serialize($item_type);
    }
    elseif ($definition instanceof MapDataDefinition) {
      $data['type'] = 'map';
      $data['properties'] = array_map([$this, 'serialize'], $definition->getPropertyDefinitions());
    }
    return $data;
  }

  /**
   * Deserializes an array into a data definition.
   *
   * @param array $definition
   *   The definition, as an array.
   */
  public function deserialize(array $definition): DataDefinitionInterface {
    if (!isset($definition['type']) || !is_string($definition['type'])) {
      throw new \InvalidArgumentException('Cannot construct PropertyDefinition if there is no type.');
    }
    $data_definition = $this->typedDataManager->createDataDefinition($definition['type']);
    if (method_exists($data_definition, 'setTypedDataManager')) {
      $data_definition->setTypedDataManager($this->typedDataManager);
    }
    if (method_exists($data_definition, 'setRequired')) {
      $data_definition->setRequired($definition['required'] ?? FALSE);
    }

    if ($data_definition instanceof ListDataDefinitionInterface) {
      // @todo open an issue to get setItemDefinition added to interface.
      // There is no reliable way to set a complex item definition, otherwise.
      if (method_exists($data_definition, 'setItemDefinition')) {
        $item_type = $definition['item_type'] ?? ['type' => 'any'];
        $item_type_definition = $this->deserialize($item_type);
        $data_definition->setItemDefinition($item_type_definition);
      }
    }
    if ($data_definition instanceof ComplexDataDefinitionInterface) {
      $map_properties = $definition['properties'] ?? [];
      // @todo open an issue about setPropertyDefinition added to interface.
      if (method_exists($data_definition, 'setPropertyDefinition')) {
        // Initialize the `properties` property as an array. Otherwise, the
        // property is null if there are no properties.
        $data_definition->getPropertyDefinitions();
        foreach ($map_properties as $map_property_name => $map_property_data) {
          $data_definition->setPropertyDefinition(
            $map_property_name,
            $this->deserialize($map_property_data)
          );
        }
      }
    }
    return $data_definition;
  }

}
