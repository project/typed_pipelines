<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\TypedData;

use Drupal\Component\Assertion\Inspector;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\typed_pipelines\Mapping\PropertyDefinition;

/**
 * Data definition for a pipeline mapping.
 */
final class PropertiesDataDefinition extends MapDataDefinition {

  /**
   * Create a new data definition from a mapping.
   *
   * @param \Drupal\typed_pipelines\Mapping\PropertyDefinition[] $mapping
   *   The mapping.
   *
   * @return self
   *   The data definition.
   */
  public static function createFromMapping(array $mapping): self {
    assert(Inspector::assertAllObjects($mapping, PropertyDefinition::class));
    $definition = self::create();
    // The root definition has to be marked as required, otherwise a map with
    // empty data is not validated properly.
    $definition->setRequired(TRUE);
    foreach ($mapping as $property) {
      $definition->setPropertyDefinition(
        $property->getPath(),
        $property->getDataDefinition()
      );
    }
    return $definition;
  }

}
