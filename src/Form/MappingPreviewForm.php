<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\typed_pipelines\Entity\Mapping;
use Drupal\typed_pipelines\Exception\ConstraintViolationException;
use Drupal\typed_pipelines\PipelineFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Provides a form to preview a process mapping.
 */
final class MappingPreviewForm extends FormBase {

  /**
   * The pipeline factory.
   *
   * @var \Drupal\typed_pipelines\PipelineFactory
   */
  protected PipelineFactory $pipelineFactory;

  /**
   * Constructs a new MappingPreviewForm object.
   *
   * @param \Drupal\typed_pipelines\PipelineFactory $pipelineFactory
   *   The pipeline factory.
   */
  public function __construct(PipelineFactory $pipelineFactory) {
    $this->pipelineFactory = $pipelineFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('typed_pipelines.pipeline_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'typed_pipelines_mapping_preview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?Mapping $typed_pipelines_mapping = NULL): array {
    if ($typed_pipelines_mapping === NULL) {
      return $form;
    }
    $form['#prefix'] = '<div id="preview-wrapper">';
    $form['#suffix'] = '</div>';
    $form['actions'] = [
      '#type' => 'actions',
      'preview' => [
        '#type' => 'button',
        '#value' => $this->t('Preview'),
        '#validate' => ['::validatePreview'],
        '#ajax' => [
          'callback' => '::doPreview',
          'wrapper' => 'preview-wrapper',
        ],
      ],
    ];
    $form['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['layout-row', 'clearfix']],
    ];
    $form['wrapper']['input'] = [
      '#title' => $this->t('Source data'),
      '#type' => 'textarea',
      '#rows' => 18,
      '#required' => TRUE,
      '#wrapper_attributes' => [
        'class' => ['layout-column', 'layout-column--half'],
      ],
    ];
    $form['wrapper']['output'] = [
      '#title' => $this->t('Sample output'),
      '#type' => 'textarea',
      '#rows' => 18,
      '#wrapper_attributes' => [
        'class' => ['layout-column', 'layout-column--half'],
      ],
    ];

    return $form;
  }

  /**
   * Validation callback for the preview.
   */
  public function validatePreview(array $form, FormStateInterface $form_state): void {
    $pipeline_mapping = $form_state->getBuildInfo()['args'][0];
    assert($pipeline_mapping instanceof Mapping);
    // Reload the entity, as it was loaded from cache in the form state.
    $pipeline_mapping = Mapping::load($pipeline_mapping->id());

    $input = $form_state->getValue('input');
    if (!is_string($input) || $input === '') {
      return;
    }
    $input = Json::decode($input);
    if (!is_array($input)) {
      $form_state->setErrorByName('input', (string) $this->t('Source is not a JSON object or array.'));
      return;
    }
    try {
      $pipeline = $this->pipelineFactory->create($pipeline_mapping->getMapping());
      $result = $pipeline->process($input);
      // Direct usage of \json_encode over Json::encode to perform a pretty
      // print of the result.
      $result = \json_encode($result->getValue(), JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
    catch (ConstraintViolationException $exception) {
      $result = '';
      foreach ($exception->getViolations() as $violation) {
        assert($violation instanceof ConstraintViolationInterface);
        $message = sprintf('%s: %s', $violation->getPropertyPath() ?: 'source', $violation->getMessage());
        $form_state->setError($form, $message);
      }
    }
    $form_state->set('pipeline_result', $result);
  }

  /**
   * AJAX callback for the preview.
   */
  public function doPreview(array $form, FormStateInterface $form_state): array {
    $form['wrapper']['output']['#value'] = $form_state->get('pipeline_result');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // noop.
  }

}
