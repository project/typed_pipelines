<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\typed_pipelines\Entity\Mapping;

/**
 * Mapping form for a pipeline.
 */
final class MappingForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $mapping = $this->entity;
    assert($mapping instanceof Mapping);
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $mapping->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $mapping->id(),
      '#machine_name' => [
        'exists' => '\Drupal\typed_pipelines\Entity\Mapping::load',
      ],
      '#disabled' => !$mapping->isNew(),
    ];

    if ($mapping->isNew()) {
      $form['mapping'] = [
        '#markup' => $this->t('<p>Save the mapping to begin adding properties.</p>'),
      ];
      return $form;
    }

    // @todo static::ajaxSubmit() requires data-drupal-selector to be the same
    //   between the various Ajax requests. A bug in
    //   \Drupal\Core\Form\FormBuilder prevents that from happening unless
    //   $form['#id'] is also the same. Normally, #id is set to a unique HTML
    //   ID via Html::getUniqueId(), but here we bypass that in order to work
    //   around the data-drupal-selector bug. This is okay so long as we
    //   assume that this form only ever occurs once on a page. Remove this
    //   workaround in https://www.drupal.org/node/2897377.
    $form['#id'] = Html::getId($form_state->getBuildInfo()['form_id']);
    $form['mapping'] = [
      '#type' => 'table',
      // Turn off input processing for this table, it is used purely for
      // presentational purposes.
      '#input' => FALSE,
      '#header' => [
        ['data' => $this->t('Source')],
        ['data' => $this->t('Destination')],
        ['data' => $this->t('Manipulators')],
        ['data' => $this->t('Operations')],
      ],
      '#empty' => $this->t('No properties defined.'),
    ];

    foreach ($mapping->getMapping() as $index => $mapping_property) {
      $property_form = &$form['mapping'][$index];
      $property_form['source_path'] = [
        '#markup' => '<code>' . $mapping_property['source']['path'] . '</code> (' . $mapping_property['source']['definition']['type'] . ')',
      ];
      $property_form['destination_path'] = [
        '#markup' => '<code>' . $mapping_property['destination']['path'] . '</code> (' . $mapping_property['destination']['definition']['type'] . ')',
      ];
      $property_form['manipulators'] = ['#markup' => implode(', ', array_column($mapping_property['manipulators'] ?? [], 'id'))];

      $property_form['operations'] = [
        '#type' => 'operations',
        '#links' => [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('entity.typed_pipelines_mapping.property_form', [
              'typed_pipelines_mapping' => $mapping->id(),
              'index' => $index,
            ]),
            'attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode([
                'width' => '95%',
              ]),
            ],
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('entity.typed_pipelines_mapping.property_delete_form', [
              'typed_pipelines_mapping' => $mapping->id(),
              'index' => $index,
            ]),
            'attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode([
                'width' => 800,
              ]),
            ],
          ],
        ],
      ];
    }

    $form['add_new'] = [
      '#type' => 'link',
      '#title' => $this->t('Add new property'),
      '#url' => Url::fromRoute('entity.typed_pipelines_mapping.property_form', [
        'typed_pipelines_mapping' => $mapping->id(),
        'index' => '_new',
      ]),
      '#attributes' => [
        'class' => [
          'button', 'use-ajax',
        ],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 700,
        ]),
      ],
    ];

    return $form;
  }

  /**
   * Callback for adding a property.
   */
  public function addPropertySubmit(array $form, FormStateInterface $form_state): void {
    // @todo implement this.
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $is_new = $this->entity->isNew();
    parent::save($form, $form_state);
    if ($is_new) {
      $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
      $this->messenger()->addStatus($this->t('Add properties to your mapping'));
    }
    else {
      $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    }
  }

}
