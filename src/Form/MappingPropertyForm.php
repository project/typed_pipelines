<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Form;

use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\typed_pipelines\Entity\Mapping;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Property form for a mapping.
 */
final class MappingPropertyForm extends FormBase {

  use AjaxFormHelperTrait;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected EntityFormBuilderInterface $entityFormBuilder;

  /**
   * Constructs a new MappingPropertyForm object.
   *
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder.
   */
  public function __construct(EntityFormBuilderInterface $entity_form_builder) {
    $this->entityFormBuilder = $entity_form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity.form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'typed_pipelines_mapping_property_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?Mapping $typed_pipelines_mapping = NULL, ?string $index = NULL): array {
    if ($typed_pipelines_mapping === NULL || $index === NULL) {
      return $form;
    }
    if ($index === '_new') {
      $mapping_property = [];
    }
    else {
      $mapping = $typed_pipelines_mapping->getMapping();
      if (!isset($mapping[$index])) {
        return $form;
      }
      $mapping_property = $mapping[$index];
    }

    $form['index'] = [
      '#type' => 'value',
      '#value' => $index,
    ];
    $form['source'] = [
      '#type' => 'fieldset',
      '#title' => 'Source',
      '#tree' => TRUE,
      'path' => [
        '#type' => 'textfield',
        '#title' => $this->t('Source path'),
        '#description' => $this->t('The path of the property in the source value. If the value is nested, user a manipulator to retrieve the value.'),
        '#default_value' => $mapping_property['source']['path'] ?? '',
      ],
      'definition' => [
        // @todo get `typed_pipelines_data_type` working, drop YAML.
        // '#type' => 'typed_pipelines_data_type',
        '#type' => 'textarea',
        '#default_value' => Yaml::encode($mapping_property['source']['definition'] ?? []),
        '#title' => $this->t('Source definition'),
        '#description' => $this->t('The typed data definition of the source property.'),
      ],
    ];
    // This is named `dest` over `destination` due to the fact it will causes
    // Drupal to process the value as if it were the `destination` query
    // parameter.
    // @see https://www.drupal.org/project/drupal/issues/3264312
    $form['dest'] = [
      '#type' => 'fieldset',
      '#title' => 'Destination',
      '#tree' => TRUE,
      'path' => [
        '#type' => 'textfield',
        '#title' => $this->t('Destination path'),
        '#description' => $this->t('The path of the property in the destination'),
        '#default_value' => $mapping_property['destination']['path'] ?? '',
      ],
      'definition' => [
        // @todo get `typed_pipelines_data_type` working, drop YAML.
        // '#type' => 'typed_pipelines_data_type',
        '#type' => 'textarea',
        '#default_value' => Yaml::encode($mapping_property['destination']['definition'] ?? []),
        '#required' => TRUE,
        '#title' => $this->t('Destination definition'),
        '#description' => $this->t('The typed data definition of the destination property.'),
      ],
    ];
    $form['manipulators'] = [
      '#type' => 'fieldset',
      '#title' => 'Manipulators',
      'manipulators' => [
        '#type' => 'textarea',
        '#title' => $this->t('Manipulator definitions'),
        '#description' => $this->t('The manipulators.'),
        '#default_value' => Yaml::encode($mapping_property['manipulators'] ?? []),
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#op' => $index === '_new' ? 'add' : 'update',
      '#value' => $index === '_new' ? $this->t('Add') : $this->t('Update'),
      '#button_type' => 'primary',
    ];
    if ($this->isAjax()) {
      $form['actions']['submit']['#ajax']['callback'] = '::ajaxSubmit';
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $pipeline_mapping = $form_state->getBuildInfo()['args'][0];
    assert($pipeline_mapping instanceof Mapping);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $pipeline_mapping = $form_state->getBuildInfo()['args'][0];
    assert($pipeline_mapping instanceof Mapping);
    $raw_mapping = $pipeline_mapping->getMapping();
    $values = $form_state->cleanValues()->getValues();

    $property = [
      'source' => [
        'path' => $values['source']['path'],
        'definition' => Yaml::decode($values['source']['definition']),
      ],
      'destination' => [
        'path' => $values['dest']['path'],
        'definition' => Yaml::decode($values['dest']['definition']),
      ],
      'manipulators' => Yaml::decode($values['manipulators']),
    ];
    if ($values['index'] === '_new') {
      $raw_mapping[] = $property;
    }
    else {
      $raw_mapping[$values['index']] = $property;
    }
    $pipeline_mapping->setMapping($raw_mapping);
    $pipeline_mapping->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state): AjaxResponse {
    $pipeline_mapping = $form_state->getBuildInfo()['args'][0];
    assert($pipeline_mapping instanceof Mapping);
    $pipeline_form = $this->entityFormBuilder->getForm($pipeline_mapping, 'edit');
    // Reset the pipeline form's action to the `edit-form` route path, not
    // the path of our dialog.
    $pipeline_form['#action'] = $pipeline_mapping->toUrl('edit-form')->toString();

    $response = new AjaxResponse();
    $response->addCommand(new MessageCommand('Updated property.'));
    $response->addCommand(new ReplaceCommand('[data-drupal-selector="typed-pipelines-mapping-edit-form"]', $pipeline_form));
    $response->addCommand(new CloseDialogCommand());
    return $response;
  }

}
