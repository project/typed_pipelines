<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Form;

use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\typed_pipelines\Entity\Mapping;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for deleting a property from a pipeline mapping.
 */
final class MappingPropertyDeleteForm extends FormBase {

  use AjaxFormHelperTrait;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected EntityFormBuilderInterface $entityFormBuilder;

  /**
   * Constructs a new MappingPropertyForm object.
   *
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder.
   */
  public function __construct(EntityFormBuilderInterface $entity_form_builder) {
    $this->entityFormBuilder = $entity_form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity.form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'typed_pipelines_mapping_property_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?Mapping $typed_pipelines_mapping = NULL, ?string $index = NULL) {
    if ($typed_pipelines_mapping === NULL || $index === NULL) {
      return $form;
    }
    $mapping = $typed_pipelines_mapping->getMapping();
    if (!isset($mapping[$index])) {
      return $form;
    }
    $form['#title'] = $this->t('Are you sure you want to remove this property?');
    $form['index'] = [
      '#type' => 'value',
      '#value' => $index,
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#button_type' => 'primary',
    ];
    if ($this->isAjax()) {
      $form['actions']['submit']['#ajax'] = [
        'callback' => '::ajaxSubmit',
      ];
    }
    $form['#theme'] = 'confirm_form';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $pipeline_mapping = $form_state->getBuildInfo()['args'][0];
    assert($pipeline_mapping instanceof Mapping);
    $raw_mapping = $pipeline_mapping->getMapping();
    unset($raw_mapping[$form_state->getValue('index')]);
    $pipeline_mapping->setMapping($raw_mapping);
    $pipeline_mapping->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    $pipeline_mapping = $form_state->getBuildInfo()['args'][0];
    assert($pipeline_mapping instanceof Mapping);
    $pipeline_form = $this->entityFormBuilder->getForm($pipeline_mapping, 'edit');

    $response = new AjaxResponse();
    $response->addCommand(new MessageCommand('Removed property.'));
    $response->addCommand(new ReplaceCommand('[data-drupal-selector="typed-pipelines-mapping-edit-form"]', $pipeline_form));
    $response->addCommand(new CloseDialogCommand());
    return $response;
  }

}
