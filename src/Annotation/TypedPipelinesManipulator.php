<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Annotation for manipulators.
 *
 * @Annotation
 */
final class TypedPipelinesManipulator extends Plugin {

  /**
   * The ID.
   *
   * @var string
   */
  public string $id = '';

  /**
   * The label.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public TranslatableMarkup $label;

  /**
   * The data type.
   *
   * @var string
   */
  public string $data_type = '';

}
