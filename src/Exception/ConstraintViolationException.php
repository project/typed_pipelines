<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Exception that contains constraint violations.
 */
class ConstraintViolationException extends \InvalidArgumentException {

  /**
   * The constraint violation list.
   *
   * @var \Symfony\Component\Validator\ConstraintViolationListInterface
   */
  private ConstraintViolationListInterface $violations;

  /**
   * Constructs a new ConstraintViolationException object.
   *
   * @param \Symfony\Component\Validator\ConstraintViolationListInterface $violations
   *   The constraint violation list.
   */
  public function __construct(ConstraintViolationListInterface $violations) {
    parent::__construct('Constraint violation has failed.');
    $this->violations = $violations;
  }

  /**
   * Get the violations.
   *
   * @return \Symfony\Component\Validator\ConstraintViolationListInterface
   *   The violations.
   */
  public function getViolations(): ConstraintViolationListInterface {
    return $this->violations;
  }

}
