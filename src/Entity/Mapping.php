<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the pipeline mapping configuration entity.
 *
 * @ConfigEntityType(
   *   id = "typed_pipelines_mapping",
 *   label = @Translation("Pipeline mapping"),
 *   handlers = {
 *     "list_builder" = "\Drupal\typed_pipelines\Entity\MappingListBuilder",
 *     "form" = {
 *        "add" = "\Drupal\typed_pipelines\Form\MappingForm",
 *        "edit" = "\Drupal\typed_pipelines\Form\MappingForm",
 *        "delete" = "\Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer typed_pipelines_mapping",
 *   config_prefix = "pipeline_mapping",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "mapping",
 *   },
 *   links = {
 *     "add-form" = "/admin/pipelines/mapping/add",
 *     "edit-form" = "/admin/pipelines/mapping/{typed_pipelines_mapping}",
 *     "delete-form" = "/admin/pipelines/mapping/{typed_pipelines_mapping}/delete",
 *     "collection" =  "/admin/pipelines/mappings"
 *   }
 * )
 */
final class Mapping extends ConfigEntityBase {

  /**
   * The mapping ID.
   *
   * @var string|null
   */
  protected ?string $id;

  /**
   * The label.
   *
   * @var string|null
   */
  protected ?string $label;

  /**
   * The mapping.
   *
   * @var array|null
   */
  protected ?array $mapping;

  /**
   * Get the mapping's label.
   *
   * @return string
   *   The label
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * Set the mapping's label.
   *
   * @param string $label
   *   The label.
   */
  public function setLabel(string $label): self {
    $this->label = $label;
    return $this;
  }

  /**
   * Get the mapping.
   *
   * @return array
   *   The mapping.
   */
  public function getMapping(): array {
    return $this->mapping ?? [];
  }

  /**
   * Set the mapping.
   *
   * @param array $mapping
   *   The mapping.
   */
  public function setMapping(array $mapping): self {
    $this->mapping = $mapping;
    return $this;
  }

}
