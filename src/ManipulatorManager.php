<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\typed_pipelines\Annotation\TypedPipelinesManipulator;
use Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface;

/**
 * Plugin manager for manipulator plugins.
 */
final class ManipulatorManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  protected $defaults = [
    'id' => '',
    'label' => '',
    'data_type' => 'any',
  ];

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);
    foreach (['id', 'label'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new PluginException(sprintf('The data manipulator %s must define the %s property.', $plugin_id, $required_property));
      }
    }
  }

  /**
   * Constructs a new ManipulatorManager object.
   *
   * @param \Traversable $namespaces
   *   The namespaces.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/TypedPipelines',
      $namespaces,
      $module_handler,
      ManipulatorInterface::class,
      TypedPipelinesManipulator::class
    );

    $this->alterInfo('typed_pipelines_manipulator_info');
    $this->setCacheBackend($cache_backend, 'typed_pipelines_manipulator_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []): ManipulatorInterface {
    $instance = parent::createInstance($plugin_id, $configuration);
    assert($instance instanceof ManipulatorInterface);
    return $instance;
  }

}
