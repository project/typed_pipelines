<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Mapping;

use Drupal\Core\TypedData\DataDefinitionInterface;

/**
 * Property definition as part of a pipeline mapping.
 */
final class PropertyDefinition {

  /**
   * The property path.
   *
   * @var string
   */
  private string $path;

  /**
   * The property definition.
   *
   * @var \Drupal\Core\TypedData\DataDefinitionInterface
   */
  private DataDefinitionInterface $definition;

  /**
   * Constructs a new PropertyDefinition object.
   *
   * @param string $path
   *   The property path.
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   *   The property definition.
   */
  public function __construct(string $path, DataDefinitionInterface $definition) {
    $this->path = $path;
    $this->definition = $definition;
  }

  /**
   * Get the path of the property.
   *
   * @return string
   *   The path.
   */
  public function getPath(): string {
    return $this->path;
  }

  /**
   * Get the data definition of the property.
   *
   * @return \Drupal\Core\TypedData\DataDefinitionInterface
   *   The data definition.
   */
  public function getDataDefinition(): DataDefinitionInterface {
    return $this->definition;
  }

}
