<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Mapping;

/**
 * Represents a property in the pipeline mapping.
 */
final class Property {

  /**
   * The source definition.
   *
   * @var \Drupal\typed_pipelines\Mapping\PropertyDefinition
   */
  private PropertyDefinition $source;

  /**
   * The destination definition.
   *
   * @var \Drupal\typed_pipelines\Mapping\PropertyDefinition
   */
  private PropertyDefinition $destination;

  /**
   * The manipulators.
   *
   * @var \Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface[]
   */
  private array $manipulators;

  /**
   * Constructs a new Property object.
   *
   * @param PropertyDefinition $source
   *   The source definition.
   * @param PropertyDefinition $destination
   *   The destination definition.
   * @param \Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface[] $manipulators
   *   The manipulators.
   */
  public function __construct(PropertyDefinition $source, PropertyDefinition $destination, array $manipulators) {
    $this->source = $source;
    $this->destination = $destination;
    $this->manipulators = $manipulators;
  }

  /**
   * Get the source definition.
   *
   * @return \Drupal\typed_pipelines\Mapping\PropertyDefinition
   *   The definition.
   */
  public function getSource(): PropertyDefinition {
    return $this->source;
  }

  /**
   * Get the destination definition.
   *
   * @return \Drupal\typed_pipelines\Mapping\PropertyDefinition
   *   The definition.
   */
  public function getDestination(): PropertyDefinition {
    return $this->destination;
  }

  /**
   * Get the manipulators.
   *
   * @return \Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface[]
   *   The manipulators.
   */
  public function getManipulators(): array {
    return $this->manipulators;
  }

}
