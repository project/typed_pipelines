<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Element;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Serialization\Yaml;

/**
 * Provides a form element for configuring data types.
 *
 * @FormElement("typed_pipelines_data_type")
 *
 * @internal
 */
final class DataTypeConfiguration extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    return [
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#tree' => TRUE,
      '#default_value' => [
        'type' => '',
        'required' => FALSE,
      ],
      '#process' => [
        [$this, 'processDataTypeConfiguration'],
      ],
      '#after_build' => [
        [self::class, 'clearValues'],
      ],

      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Processes the data type configuration form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public function processDataTypeConfiguration(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $id_prefix = implode('-', $element['#parents']);
    $wrapper_id = Html::getUniqueId($id_prefix . '-ajax-wrapper');
    $element = [
      '#tree' => TRUE,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      // Pass the id along to other methods.
      '#wrapper_id' => $wrapper_id,
    ] + $element;

    $element['type'] = [
      '#type' => 'select',
      '#title' => t('Data type'),
      '#options' => [
        'string' => $this->t('String'),
        'integer' => $this->t('Integer'),
        'list' => $this->t('List'),
        'map' => $this->t('Map'),
      ],
      '#default_value' => $element['#default_value']['type'] ?? '',
      '#required' => $element['#required'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [self::class, 'ajaxRefresh'],
        'wrapper' => $wrapper_id,
      ],
    ];
    $element['required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Required'),
      '#default_value' => $element['#default_value']['required'],
    ];

    // The #value has the new values on #ajax, the #default_value otherwise.
    $value = $element['#value'];
    $selected_type = $value['type'] ?? '';
    if ($selected_type === 'list') {
      $element['item_type'] = [
        '#type' => 'typed_pipelines_data_type',
        '#default_value' => $value['item_type'] ?? [],
      ];
    }
    if ($selected_type === 'map') {
      $element['properties'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Properties'),
        '#default_value' => Yaml::encode($value['properties'] ?? []),
      ];
    }

    return $element;
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state): array {
    $triggering_element = $form_state->getTriggeringElement();
    assert(is_array($triggering_element));
    $parents = $triggering_element['#array_parents'];
    array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Clears dependent form values when the data type plugin ID changes.
   *
   * Implemented as an #after_build callback because #after_build runs before
   * validation, allowing the values to be cleared early enough to prevent the
   * "Illegal choice" error.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The element.
   */
  public static function clearValues(array $element, FormStateInterface $form_state): array {
    $triggering_element = $form_state->getTriggeringElement();
    if (!$triggering_element) {
      return $element;
    }
    $triggering_element_name = end($triggering_element['#parents']);
    if ($triggering_element_name === 'type') {
      $input = &$form_state->getUserInput();
      NestedArray::setValue($input, array_merge($element['#parents'], ['required']), NULL);
      $element['required']['#value'] = NULL;
    }

    return $element;
  }

}
