<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for manipulator plugins.
 */
interface ManipulatorInterface extends PluginInspectionInterface {

  /**
   * Transforms a value.
   */
  public function transform($value);

}
