<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\PluginBase;

/**
 * Provides the ability to run `array_filter` on an array of data.
 *
 * @TypedPipelinesManipulator(
 *   id="filter",
 *   label=@Translation("Filter from array."),
 *   data_type="map",
 * )
 */
class Filter extends PluginBase implements ManipulatorInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    $keys = explode('/', $this->configuration['key']);
    return array_values(array_filter($value, function (array $value) use ($keys) {
      return NestedArray::getValue($value, $keys) === $this->configuration['value'];
    }));
  }

}
