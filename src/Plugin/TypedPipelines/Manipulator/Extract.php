<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\PluginBase;

/**
 * Provides the ability to extract data from an array.
 *
 * @TypedPipelinesManipulator(
 *   id="extract",
 *   label=@Translation("Extract from array."),
 *   data_type="map",
 * )
 */
class Extract extends PluginBase implements ManipulatorInterface {

  /**
   * {@inheritdoc}
   */
  public function transform($value) {
    $key_exists = TRUE;
    // @todo should we store `key` to be an array vs `/` path?
    $keys = explode('/', $this->configuration['key']);
    $value = NestedArray::getValue($value, $keys, $key_exists);
    // @todo I dislike throwing the exception here.
    //   There should have been schema validation which determined if this was
    //   feasible to run in the first place. But it makes it possible to
    //   integrate with Migrate more easily.
    if ($key_exists === FALSE) {
      throw new \InvalidArgumentException("The key '{$this->configuration['key']}' is missing from the data.");
    }
    return $value;
  }

}
