<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines;

use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\typed_pipelines\Exception\ConstraintViolationException;
use Drupal\typed_pipelines\Mapping\Property;
use Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface;
use Drupal\typed_pipelines\TypedData\PropertiesDataDefinition;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * The pipeline.
 */
final class Pipeline {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  private TypedDataManagerInterface $typedDataManager;

  /**
   * The mapping.
   *
   * @var \Drupal\typed_pipelines\Mapping\Property[]
   */
  private array $mapping;

  /**
   * Constructs a new Pipeline object.
   *
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   * @param \Drupal\typed_pipelines\Mapping\Property[] $mapping
   *   The mapping.
   */
  public function __construct(TypedDataManagerInterface $typed_data_manager, array $mapping) {
    $this->typedDataManager = $typed_data_manager;
    $this->mapping = $mapping;
  }

  /**
   * Get the property mapping.
   *
   * @return \Drupal\typed_pipelines\Mapping\Property[]
   *   The mapping.
   */
  public function getMapping(): array {
    return $this->mapping;
  }

  /**
   * Gets the source properties definition.
   *
   * @return \Drupal\typed_pipelines\TypedData\PropertiesDataDefinition
   *   The definition.
   */
  public function getSourceDefinition(): PropertiesDataDefinition {
    return PropertiesDataDefinition::createFromMapping(
      array_map(static fn (Property $property) => $property->getSource(), $this->mapping)
    );
  }

  /**
   * Gets the destination properties definition.
   *
   * @return \Drupal\typed_pipelines\TypedData\PropertiesDataDefinition
   *   The definition.
   */
  public function getDestinationDefinition(): PropertiesDataDefinition {
    return PropertiesDataDefinition::createFromMapping(
      array_map(static fn (Property $property) => $property->getDestination(), $this->mapping)
    );
  }

  /**
   * Processes data in the pipeline.
   *
   * @param array $data
   *   The data.
   */
  public function process(array $data): TypedDataInterface {
    $processed = [];

    $source_value = $this->typedDataManager->create(
      $this->getSourceDefinition(),
      $data
    );
    assert($source_value instanceof ComplexDataInterface);
    $this->validateData($source_value);

    foreach ($this->mapping as $item) {
      $processed[$item->getDestination()->getPath()] = array_reduce(
        $item->getManipulators(),
        // This is where data should be validated against annotation.
        // Is there a way to validate w/o being at runtime (preflight check.)
        // We need a post-condition / handoff check.
        // Some way to specify the expected output
        // - (filter => array to array)
        // - (extract => array to string|int|bool|mixed)
        // Allow specifying expected return.
        // DEFINITION: incoming type
        // CONFIGURATION: outgoing type.
        static fn ($carry, ManipulatorInterface $manipulator) => $manipulator->transform($carry),
        // We should be passing in typed data to the transformers.
        $source_value->get($item->getSource()->getPath())->getValue()
      );
    }

    $destination_value = $this->typedDataManager->create(
      $this->getDestinationDefinition(),
      $processed
    );
    $this->validateData($destination_value);
    return $destination_value;
  }

  /**
   * Validate the data throw a constraint violation exception if needed.
   */
  private function validateData(TypedDataInterface $data): void {
    try {
      $violations = $data->validate();
      if (count($violations) > 0) {
        throw new ConstraintViolationException($violations);
      }
    }
    catch (ConstraintViolationException $exception) {
      throw $exception;
    }
    // Try to handle invalid arguments caused by Map and ItemList.
    catch (\InvalidArgumentException $exception) {
      self::handleInvalidArgumentException($exception);
    }
  }

  /**
   * Handle an \InvalidArgumentException thrown by RecursiveContextualValidator.
   *
   * @param \InvalidArgumentException $exception
   *   The exception.
   *
   * @see https://www.drupal.org/project/drupal/issues/3254374
   */
  private static function handleInvalidArgumentException(\InvalidArgumentException $exception): void {
    $message = $exception->getMessage();

    $trace = $exception->getTrace();
    $get_property_instance_call = $trace[1] ?? NULL;
    // Something went wrong, bubble the exception.
    if (
      $get_property_instance_call === NULL
      || $get_property_instance_call['function'] !== 'getPropertyInstance'
      || !is_array($get_property_instance_call['args'])
      || count($get_property_instance_call['args']) !== 3
    ) {
      throw $exception;
    }
    [$definition_object, $property_name, $property_value] = $get_property_instance_call['args'];
    assert($definition_object instanceof TypedDataInterface);
    $violations = new ConstraintViolationList([
      new ConstraintViolation($message, NULL, [], '', $property_name, $property_value),
    ]);
    throw new ConstraintViolationException($violations);
  }

}
