# Data Pipelines

A low-level API that integrates with the Typed Data API to provide data processing and validation.

The module provides a queued processing mechanism, as well.

## Pipeline

A pipeline processes a source data against a mapping and returns the processed result.

## Mapping

A mapping is a collection of definitions that represent:

* The source property
* The destination property
* The manipulators to transform and process the value.

## Drupal core issues / wishlist

* [RecursiveContextualValidator should handle exceptions when validating nodes recursively](https://www.drupal.org/project/drupal/issues/3254374)
* [Add missing type hint to TypedDataInterface::createInstance() method](https://www.drupal.org/project/drupal/issues/3092651)
* Setters on `DataDefinition`, `ListDataDefinition`, and `MapDataDefinition` are not on any interfaces.
* [`ListDataDefinition` is useless with `toArray`](https://www.drupal.org/project/drupal/issues/3254831).
* `MapDataDefinition` does not embed its properties in `toArray`
* Interface for `\Drupal\Core\TypedData\TypedDataTrait`, like `TypedDataManagerAwareInterface`.
* `\Drupal\Core\TypedData\ComplexDataDefinitionBase::$propertyDefinitions` should default to `[]` not `null`.
* [Map data definition properties are not validated if its empty](https://www.drupal.org/project/drupal/issues/3256536)

Interface replaces this:
```php
# \Drupal\Core\TypedData\TypedDataManager::createDataDefinition
if (method_exists($data_definition, 'setTypedDataManager')) {
  $data_definition->setTypedDataManager($this);
}
```

List `toArray` problems. This fails as `type` is missing.

```php
self::assertEquals(
  ['type' => 'list', 'required' => TRUE],
  $sut->serialize(
    ListDataDefinition::createFromItemType('string')
      ->setRequired(TRUE)
  ),
);
```
