<?php

declare(strict_types=1);

namespace Drupal\Tests\typed_pipelines\Kernel;

/**
 * Tests the mapping configuration entity.
 *
 * @group typed_pipelines
 */
final class PipelineMappingConfigEntityTest extends TypedPipelinesTestBase {

  /**
   * Test the entity.
   */
  public function testEntity(): void {
    $etm = $this->container->get('entity_type.manager');
    self::assertTrue($etm->hasDefinition('typed_pipelines_mapping'));
  }

}
