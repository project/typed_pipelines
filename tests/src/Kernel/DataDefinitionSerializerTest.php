<?php

declare(strict_types=1);

namespace Drupal\Tests\typed_pipelines\Kernel;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\typed_pipelines\Entity\Mapping;

/**
 * Tests the data definition serializer.
 *
 * @group typed_pipelines
 * @coversDefaultClass \Drupal\typed_pipelines\TypedData\DataDefinitionSerializer
 */
final class DataDefinitionSerializerTest extends TypedPipelinesTestBase {

  /**
   * Tests deserialization of a data definition.
   *
   * @covers ::deserialize
   */
  public function testDeserialize(): void {
    $tdm = $this->container->get('typed_data_manager');
    $sut = $this->container->get('typed_pipelines.data_definition_serializer');

    self::assertEquals(
      DataDefinition::create('string')
        ->setRequired(FALSE)
        ->setTypedDataManager($tdm),
      $sut->deserialize(['type' => 'string'])
    );
    self::assertEquals(
      DataDefinition::create('string')
        ->setRequired(TRUE)
        ->setTypedDataManager($tdm),
      $sut->deserialize(['type' => 'string', 'required' => TRUE])
    );

    self::assertEquals(
      MapDataDefinition::create()
        ->setRequired(FALSE)
        ->setTypedDataManager($tdm)
        ->setPropertyDefinition('name', DataDefinition::create('string')
          ->setTypedDataManager($tdm)
          ->setRequired(TRUE))
        ->setPropertyDefinition('bundle', DataDefinition::create('string')
          ->setTypedDataManager($tdm)
          ->setRequired(FALSE))
        ->setPropertyDefinition('url', DataDefinition::create('uri')
          ->setTypedDataManager($tdm)
          ->setRequired(FALSE)),
      $sut->deserialize([
        'type' => 'map',
        'required' => FALSE,
        'properties' => [
          'name' => ['type' => 'string', 'required' => TRUE],
          'bundle' => ['type' => 'string'],
          'url' => ['type' => 'uri'],
        ],
      ])
    );
  }

  /**
   * Tests serialization of a data definition.
   *
   * @covers ::serialize
   */
  public function testSerialize(): void {
    $sut = $this->container->get('typed_pipelines.data_definition_serializer');

    self::assertEquals(
      ['type' => 'string', 'required' => FALSE],
      $sut->serialize(
        DataDefinition::create('string')
          ->setRequired(FALSE)
      ),
    );
    self::assertEquals(
      ['type' => 'string', 'required' => TRUE],
      $sut->serialize(
        DataDefinition::create('string')
          ->setRequired(TRUE)
      ),
    );

    self::assertEquals(
      [
        'type' => 'list',
        'required' => TRUE,
        'item_type' => [
          'type' => 'string',
        ],
      ],
      $sut->serialize(
        ListDataDefinition::createFromItemType('string')
          ->setRequired(TRUE)
      ),
    );
    self::assertEquals(
      [
        'type' => 'map',
        'required' => FALSE,
        'properties' => [
          'name' => [
            'type' => 'string',
            'required' => TRUE,
          ],
          'bundle' => [
            'type' => 'string',
            'required' => FALSE,
          ],
          'url' => [
            'type' => 'uri',
            'required' => FALSE,
          ],
        ],
      ],
      $sut->serialize(
        MapDataDefinition::create()
          ->setRequired(FALSE)
          ->setPropertyDefinition('name', DataDefinition::create('string')
            ->setRequired(TRUE))
          ->setPropertyDefinition('bundle', DataDefinition::create('string')
            ->setRequired(FALSE))
          ->setPropertyDefinition('url', DataDefinition::create('uri')
            ->setRequired(FALSE))
      ),
    );
  }

  /**
   * Tests the serialization within a pipeline.
   */
  public function testWithPipeline(): void {
    $sut = $this->container->get('typed_pipelines.data_definition_serializer');
    $pipeline_factory = $this->container->get('typed_pipelines.pipeline_factory');

    $config = Mapping::load('pokemon_species');
    self::assertNotNull($config);
    $raw = $config->getMapping();
    $pipeline = $pipeline_factory->create($raw);
    $mapping = $pipeline->getMapping();

    foreach ($raw as $index => $item) {
      self::assertEquals(
        $item['source']['definition'],
        $sut->serialize($mapping[$index]->getSource()->getDataDefinition())
      );
      self::assertEquals(
        $mapping[$index]->getSource()->getDataDefinition(),
        $sut->deserialize($item['source']['definition'])
      );
    }

  }

}
