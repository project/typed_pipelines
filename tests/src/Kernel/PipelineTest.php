<?php

declare(strict_types=1);

namespace Drupal\Tests\typed_pipelines\Kernel;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\TypedData\ListDataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\typed_pipelines\Exception\ConstraintViolationException;
use Drupal\typed_pipelines\Mapping\Property;

/**
 * Tests pipeline processing.
 *
 * @group typed_pipelines
 */
final class PipelineTest extends TypedPipelinesTestBase {

  /**
   * Tests parsing a mapping.
   *
   * @param array $mapping
   *   The mapping.
   *
   * @dataProvider mappingData
   */
  public function testParse(array $mapping): void {
    self::assertNotNull($this->pipelineFactory);
    $pipeline = $this->pipelineFactory->create($mapping);
    $parsed = $pipeline->getMapping();
    self::assertCount(2, $parsed);
    $row = $parsed[0];
    self::assertInstanceOf(Property::class, $row);
    self::assertEquals('names', $row->getSource()->getPath());
    $names_data_definition = $row->getSource()->getDataDefinition();
    assert($names_data_definition instanceof ListDataDefinition);
    self::assertEquals('map', $names_data_definition->getItemDefinition()->getDataType());
    $names_data_mapping_definition = $names_data_definition->getItemDefinition();
    assert($names_data_mapping_definition instanceof MapDataDefinition);
    $property_definitions = $names_data_mapping_definition->getPropertyDefinitions();
    self::assertEquals(['language', 'name'], array_keys($property_definitions));

    self::assertEquals('name', $row->getDestination()->getPath());
    self::assertEquals('string', $row->getDestination()->getDataDefinition()->getDataType());
    self::assertCount(2, $row->getManipulators());
  }

  /**
   * Tests validation of the source data definition.
   *
   * @param array $mapping
   *   The mapping.
   * @param array $source
   *   The source data.
   * @param array $expected
   *   The expected result.
   *
   * @dataProvider invalidMappingData
   */
  public function testSourceDataDefinition(array $mapping, array $source, array $expected): void {
    self::assertNotNull($this->pipelineFactory);
    try {
      $pipeline = $this->pipelineFactory->create($mapping);
      $pipeline->process($source);
      $this->fail('Expected ConstraintViolationException');
    }
    catch (ConstraintViolationException $e) {
      $violations = $e->getViolations();
      self::assertCount(count($expected), $violations);
      foreach ($expected as $offset => $item) {
        $violation = $violations->get($offset);
        self::assertEquals($item, [
          'message' => (string) $violation->getMessage(),
          'path' => $violation->getPropertyPath(),
        ]);
      }
    }

  }

  /**
   * Tests mapping data.
   *
   * @param array $mapping
   *   The mapping.
   * @param string $source
   *   The source data.
   * @param array $expected
   *   The expected result.
   *
   * @dataProvider mappingData
   */
  public function testMapping(array $mapping, string $source, array $expected): void {
    self::assertNotNull($this->pipelineFactory);
    $contents = file_get_contents($source);
    self::assertNotFalse($contents);
    $source = Json::decode($contents);
    self::assertIsArray($source);
    $pipeline = $this->pipelineFactory->create($mapping);
    $result = $pipeline->process($source);
    self::assertEquals($expected, $result->getValue());
  }

  /**
   * Data provider for verifying valid mapping data.
   *
   * @phpstan-ignore-next-line
   */
  public function mappingData(): \Generator {
    $fixture_path = __DIR__ . '/../../modules/typed_pipelines_test/config/install/typed_pipelines.pipeline_mapping.pokemon_species.yml';
    $default_name_mapping = Yaml::decode(file_get_contents($fixture_path))['mapping'];
    yield [
      $default_name_mapping,
      __DIR__ . '/../../fixtures/pokemon-species/1.json',
      [
        'name' => 'Bulbasaur',
        'description' => "It carries a seed\non its back right\nfrom birth. As it\fgrows older, the\nseed also grows\nlarger.",
      ],
    ];
  }

  /**
   * Data provider for verifying invalid mapping data.
   *
   * @phpstan-ignore-next-line
   */
  public function invalidMappingData(): \Generator {
    $fixture_path = __DIR__ . '/../../modules/typed_pipelines_test/config/install/typed_pipelines.pipeline_mapping.pokemon_species.yml';
    $default_name_mapping = Yaml::decode(file_get_contents($fixture_path))['mapping'];
    yield 'missing source field' => [
      $default_name_mapping,
      [
        'unmapped' => '',
      ],
      [
        [
          'path' => 'names',
          'message' => 'This value should not be null.',
        ],
        [
          'path' => 'flavor_text_entries',
          'message' => 'This value should not be null.',
        ],
      ],
    ];
    yield 'data is empty' => [
      $default_name_mapping,
      [],
      [
        [
          'path' => '',
          'message' => 'This value should not be null.',
        ],
      ],
    ];
    yield 'source field is string' => [
      $default_name_mapping,
      [
        'names' => '',
      ],
      [
        [
          'path' => 'names',
          'message' => 'Cannot set a list with a non-array value.',
        ],
      ],
    ];
    yield 'source field is empty array' => [
      $default_name_mapping,
      [
        'names' => [],
      ],
      [
        [
          'path' => 'names',
          'message' => 'This value should not be null.',
        ],
        [
          'path' => 'flavor_text_entries',
          'message' => 'This value should not be null.',
        ],
      ],
    ];
    yield 'source field is array but wrong schema' => [
      $default_name_mapping,
      [
        'flavor_text_entries' => [],
        'names' => [
          [
            'name' => 'foo',
          ],
        ],
      ],
      [
        [
          'path' => 'names.0.language',
          'message' => 'This value should not be null.',
        ],
        [
          'path' => 'flavor_text_entries',
          'message' => 'This value should not be null.',
        ],
      ],
    ];
  }

}
