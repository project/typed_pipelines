<?php

declare(strict_types=1);

namespace Drupal\Tests\typed_pipelines\Kernel;

use Drupal\typed_pipelines_migrate\Plugin\Migrate\Process\ProcessUsesManipulatorInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrateProcessInterface;
use Drupal\migrate\Row;

/**
 * Tests experimental concept of Migrate integration.
 *
 * @group typed_pipelines
 */
final class MigrateIntegrationTest extends TypedPipelinesTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate',
    'typed_pipelines_migrate',
  ];

  /**
   * Tests decoration of plugins.
   */
  public function testDecoration() {
    $plugin = $this->container->get('plugin.manager.migrate.process')->createInstance(
      'extract',
      ['index' => ['und', 0, 'value']]
    );
    assert($plugin instanceof MigrateProcessInterface);
    self::assertInstanceOf(ProcessUsesManipulatorInterface::class, $plugin);
    $value = [
      'field_foobar' => [
        'und' => [
          ['value' => 'baz'],
        ],
      ],
    ];
    $result = $plugin->transform(
      $value['field_foobar'],
      $this->prophesize(MigrateExecutableInterface::class)->reveal(),
      new Row([], []),
      'abcd'
    );
    self::assertEquals('baz', $result);
  }

}
