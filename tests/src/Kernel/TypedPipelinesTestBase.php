<?php

declare(strict_types=1);

namespace Drupal\Tests\typed_pipelines\Kernel;

use Drupal\typed_pipelines\PipelineFactory;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Base test case class for TypedPipelines.
 */
abstract class TypedPipelinesTestBase extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'typed_pipelines',
    'typed_pipelines_test',
  ];

  /**
   * The pipeline factory.
   *
   * @var \Drupal\typed_pipelines\PipelineFactory|null
   *
   * @note set to nullable due to Drupal tearing down non-private properties.
   */
  protected ?PipelineFactory $pipelineFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->pipelineFactory = $this->container->get('typed_pipelines.pipeline_factory');
    $this->installConfig(['typed_pipelines_test']);
  }

}
