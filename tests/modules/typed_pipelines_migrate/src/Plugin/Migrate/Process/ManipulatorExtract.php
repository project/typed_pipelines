<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines_migrate\Plugin\Migrate\Process;

use Drupal\Component\Utility\Variable;
use Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\migrate\process\Extract;
use Drupal\migrate\Row;

/**
 * Decorated extract plugin.
 */
class ManipulatorExtract extends Extract implements ProcessUsesManipulatorInterface {

  /**
   * The manipulator.
   *
   * @var \Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface
   */
  private ManipulatorInterface $manipulator;

  /**
   * {@inheritdoc}
   */
  public function getManipulatorId(): string {
    return 'extract';
  }

  /**
   * {@inheritdoc}
   */
  public function getManipulatorConfiguration(): array {
    return [
      'key' => implode('/', $this->configuration['index']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setManipulator(ManipulatorInterface $manipulator): void {
    $this->manipulator = $manipulator;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    try {
      // Delegate to manipulator plugin.
      return $this->manipulator->transform($value);
    }
    // Handle missing key and fallback to default.
    catch (\InvalidArgumentException $exception) {
      if (array_key_exists('default', $this->configuration)) {
        return $this->configuration['default'];
      }
      throw new MigrateException(
        sprintf("Array index missing, extraction failed for '%s'. Consider adding a `default` key to the configuration.", Variable::export($value)),
      0,
        $exception
      );
    }
  }

}
