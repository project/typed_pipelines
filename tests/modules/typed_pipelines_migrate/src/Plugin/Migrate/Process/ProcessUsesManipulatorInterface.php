<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines_migrate\Plugin\Migrate\Process;

use Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface;
use Drupal\migrate\Plugin\MigrateProcessInterface;

/**
 * Interface for Migrate plugins which leverage Typed Pipelines manipulators.
 */
interface ProcessUsesManipulatorInterface extends MigrateProcessInterface {

  /**
   * Get the manipulator plugin ID.
   */
  public function getManipulatorId(): string;

  /**
   * Get the manipulator plugin's configuration based on the process plugin.
   *
   * @return array
   *   The configuration.
   */
  public function getManipulatorConfiguration(): array;

  /**
   * Set the manipulator plugin onto the process plugin.
   *
   * @param \Drupal\typed_pipelines\Plugin\TypedPipelines\Manipulator\ManipulatorInterface $manipulator
   *   The manipulator.
   */
  public function setManipulator(ManipulatorInterface $manipulator): void;

}
