<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines_migrate\Plugin\Migrate;

use Drupal\typed_pipelines\ManipulatorManager;
use Drupal\typed_pipelines_migrate\Plugin\Migrate\Process\ProcessUsesManipulatorInterface;
use Drupal\migrate\Plugin\MigratePluginManagerInterface;
use Drupal\migrate\Plugin\MigrateProcessInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Decorated proccess plugin manager to inject manipulators.
 */
final class ProcessPluginManager implements MigratePluginManagerInterface {

  /**
   * The decorated process plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigratePluginManagerInterface
   */
  private MigratePluginManagerInterface $inner;

  /**
   * The manipulator manager.
   *
   * @var \Drupal\typed_pipelines\ManipulatorManager
   */
  private ManipulatorManager $manipulatorManager;

  /**
   * Constructs a new ProcessPluginManager object.
   *
   * @param \Drupal\migrate\Plugin\MigratePluginManagerInterface $inner
   *   The decorated process plugin manager.
   * @param \Drupal\typed_pipelines\ManipulatorManager $manipulatorManager
   *   The manipulator manager.
   */
  public function __construct(MigratePluginManagerInterface $inner, ManipulatorManager $manipulatorManager) {
    $this->inner = $inner;
    $this->manipulatorManager = $manipulatorManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition($plugin_id, $exception_on_invalid = TRUE) {
    return $this->inner->getDefinition($plugin_id, $exception_on_invalid);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions(): array {
    return $this->inner->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function hasDefinition($plugin_id): bool {
    return $this->inner->hasDefinition($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    return $this->inner->getInstance($options);
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = [], MigrationInterface $migration = NULL): MigrateProcessInterface {
    $plugin = $this->inner->createInstance($plugin_id, $configuration, $migration);
    assert($plugin instanceof MigrateProcessInterface);

    if ($plugin instanceof ProcessUsesManipulatorInterface) {
      $manipulator = $this->manipulatorManager->createInstance(
        $plugin->getManipulatorId(),
        $plugin->getManipulatorConfiguration()
      );
      $plugin->setManipulator($manipulator);
    }
    return $plugin;
  }

}
