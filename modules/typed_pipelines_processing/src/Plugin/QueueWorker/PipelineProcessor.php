<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines_processing\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\typed_pipelines\Entity\Mapping;
use Drupal\typed_pipelines_processing\Events\ProcessingFinishedEvent;
use Drupal\typed_pipelines_processing\Events\ProcessingRequestEvent;
use Drupal\typed_pipelines\PipelineFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Queue worker to run pipelines.
 *
 * @QueueWorker(
 *   id = "typed_pipelines_processing_pipeline_processor",
 *   title = @Translation("Pipeline Processor"),
 *   cron = {"time" = 60}
 * )
 *
 * @todo needs a deriver so that each pipeline does not block others on failure.
 */
final class PipelineProcessor extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The pipeline factory.
   *
   * @var \Drupal\typed_pipelines\PipelineFactory
   */
  private PipelineFactory $pipelineFactory;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private EventDispatcherInterface $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = new self($configuration, $plugin_id, $plugin_definition);
    $instance->pipelineFactory = $container->get('typed_pipelines.pipeline_factory');
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    assert($data instanceof ProcessingRequestEvent);

    $pipeline_data = Mapping::load($data->getPipelineId());
    if ($pipeline_data === NULL) {
      throw new SuspendQueueException("The pipeline `{$data->getPipelineId()}` cannot be found.");
    }

    $pipeline = $this->pipelineFactory->create($pipeline_data->getMapping());
    // Allow the constraint exception to be thrown and bubble up.
    // Or should we catch and fire a ProcessingFailureEvent?
    $result = $pipeline->process($data->getData());
    $this->eventDispatcher->dispatch(new ProcessingFinishedEvent(
      $data->getPipelineId(),
      $pipeline,
      $result,
      $data->getData()
    ));
  }

}
