<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines_processing\EventSubscriber;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\typed_pipelines\Entity\Mapping;
use Drupal\typed_pipelines_processing\Events\ProcessingRequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber to processing requests for a pipeline.
 */
final class ProcessingRequestSubscriber implements EventSubscriberInterface {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private QueueFactory $queueFactory;

  /**
   * Constructs a new ProcessingRequestSubscriber object.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   */
  public function __construct(QueueFactory $queueFactory) {
    $this->queueFactory = $queueFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ProcessingRequestEvent::class => 'queuePipeline',
    ];
  }

  /**
   * Queue a pipeline for processing.
   *
   * @param \Drupal\typed_pipelines_processing\Events\ProcessingRequestEvent $event
   *   The event.
   */
  public function queuePipeline(ProcessingRequestEvent $event): void {
    $pipeline_data = Mapping::load($event->getPipelineId());
    if ($pipeline_data === NULL) {
      throw new \InvalidArgumentException("The pipeline `{$event->getPipelineId()}` cannot be found.");
    }

    $queue = $this->getQueue($event->getPipelineId());
    $queue->createItem($event);
  }

  /**
   * Get a pipeline's queue.
   *
   * @param string $pipeline_id
   *   The pipeline ID.
   *
   * @return \Drupal\Core\Queue\QueueInterface
   *   The queue.
   */
  private function getQueue(string $pipeline_id): QueueInterface {
    // @todo support a derivative by pipeline_id.
    $plugin_id = 'typed_pipelines_processing_pipeline_processor';
    return $this->queueFactory->get($plugin_id);
  }

}
