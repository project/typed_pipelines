<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines_processing\Events;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\typed_pipelines\Pipeline;

/**
 * Event dispatched when processing of a pipeline has finished.
 */
final class ProcessingFinishedEvent extends Event {

  /**
   * The pipeline ID.
   *
   * @var string
   */
  private string $pipelineId;

  /**
   * The pipeline.
   *
   * @var \Drupal\typed_pipelines\Pipeline
   */
  private Pipeline $pipeline;

  /**
   * The pipeline result value.
   *
   * @var \Drupal\Core\TypedData\TypedDataInterface
   */
  private TypedDataInterface $result;

  /**
   * The source data.
   *
   * @var array
   */
  private array $source;

  /**
   * Constructs a new ProcessingFinishedEvent object.
   *
   * @param string $pipeline_id
   *   The pipeline ID.
   * @param \Drupal\typed_pipelines\Pipeline $pipeline
   *   The pipeline.
   * @param \Drupal\Core\TypedData\TypedDataInterface $result
   *   The pipeline result value.
   * @param mixed[] $source
   *   The source data.
   */
  public function __construct(string $pipeline_id, Pipeline $pipeline, TypedDataInterface $result, array $source) {
    $this->pipelineId = $pipeline_id;
    $this->pipeline = $pipeline;
    $this->result = $result;
    $this->source = $source;
  }

  /**
   * Get the pipeline ID.
   *
   * @return string
   *   The pipeline.
   */
  public function getPipelineId(): string {
    return $this->pipelineId;
  }

  /**
   * Get the pipeline.
   *
   * @return \Drupal\typed_pipelines\Pipeline
   *   The pipeline.
   */
  public function getPipeline(): Pipeline {
    return $this->pipeline;
  }

  /**
   * Get the result.
   *
   * @return \Drupal\Core\TypedData\TypedDataInterface
   *   The result.
   */
  public function getResult(): TypedDataInterface {
    return $this->result;
  }

  /**
   * Get the source data.
   *
   * @return array
   *   The source data.
   */
  public function getSource(): array {
    return $this->source;
  }

}
