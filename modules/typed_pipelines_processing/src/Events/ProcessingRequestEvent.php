<?php

declare(strict_types=1);

namespace Drupal\typed_pipelines_processing\Events;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event dispatched to request processing of a pipeline.
 */
final class ProcessingRequestEvent extends Event {

  /**
   * The pipeline ID.
   *
   * @var string
   */
  private string $pipelineId;

  /**
   * The data.
   *
   * @var array
   */
  private array $data;

  /**
   * Constructs a new ProcessingRequestEvent object.
   *
   * @param string $pipeline_id
   *   The pipeline ID.
   * @param array $data
   *   The source data.
   */
  public function __construct(string $pipeline_id, array $data) {
    $this->pipelineId = $pipeline_id;
    $this->data = $data;
  }

  /**
   * Get the pipeline ID.
   *
   * @return string
   *   The pipeline ID.
   */
  public function getPipelineId(): string {
    return $this->pipelineId;
  }

  /**
   * Get the source data.
   *
   * @return array
   *   The source data.
   */
  public function getData(): array {
    return $this->data;
  }

}
