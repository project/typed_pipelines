# Typed Pipelines: Processing

Provides a mechanism for requesting a piece of data to be processed by a pipeline. When the pipeline has been processed,
and event is dispatched that allows handling the resulting data.

## Queueable processing

* Dispatch an `ProcessingRequestEvent` event with the pipeline mapping ID and source data.
* This module subscribes to the processing request event and places it into its queue.
* The queue worker runs the pipeline.
* When finished, the `ProcessingFinishedEvent` event is dispatched.
* This allows for storing the processed data as desired.
* If there is an error, the queue logs exceptions (such as invalid schema).

TODO: errors should probably have their own `ProcessingFailureEvent` that is dispatched.
