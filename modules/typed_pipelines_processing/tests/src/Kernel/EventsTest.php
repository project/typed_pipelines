<?php

declare(strict_types=1);

namespace Drupal\Tests\typed_pipelines_processing\Kernel;

use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\typed_pipelines\Kernel\TypedPipelinesTestBase;
use Drupal\typed_pipelines_processing\Events\ProcessingFinishedEvent;
use Drupal\typed_pipelines_processing\Events\ProcessingRequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Tests the events provided by typed_pipelines.
 *
 * @group typed_pipelines
 * @group typed_pipelines_processing
 */
final class EventsTest extends TypedPipelinesTestBase implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'typed_pipelines_processing',
  ];

  /**
   * Tracked finished pipelines.
   *
   * @var \Drupal\typed_pipelines_processing\Events\ProcessingFinishedEvent[]
   */
  private array $finishedPipelines = [];

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ProcessingFinishedEvent::class => 'trackFinishedPipelines',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    parent::register($container);
    // Register ourselves as an event subscriber.
    $container
      ->register('testing.pipelines_event_subscriber', self::class)
      ->addTag('event_subscriber');
    $container->set('testing.pipelines_event_subscriber', $this);
  }

  /**
   * Tests the event for requesting processing of a pipeline.
   */
  public function testProcessingRequestEvent(): void {
    $event = new ProcessingRequestEvent('foo', ['bar' => 'baz']);
    self::assertEquals('foo', $event->getPipelineId());
    self::assertEquals(['bar' => 'baz'], $event->getData());
  }

  /**
   * Tests when an invalid pipeline is provided.
   */
  public function testEventWithInvalidPipeline(): void {
    $event = new ProcessingRequestEvent('foo', ['bar' => 'baz']);
    $this->expectException(\InvalidArgumentException::class);
    $this->expectExceptionMessage("The pipeline `{$event->getPipelineId()}` cannot be found.");
    $this->container->get('event_dispatcher')->dispatch($event);
  }

  /**
   * Tests the processing request subscriber.
   */
  public function testProcessingRequestSubscriber(): void {
    $contents = file_get_contents(__DIR__ . '/../../../../../tests/fixtures/pokemon-species/1.json');
    self::assertNotFalse($contents);
    $event = new ProcessingRequestEvent(
      'pokemon_species',
      Json::decode($contents)
    );
    $this->container->get('event_dispatcher')->dispatch($event);

    $queue = $this->container->get('queue')->get('typed_pipelines_processing_pipeline_processor');
    self::assertEquals(1, $queue->numberOfItems());
    $this->container->get('cron')->run();
    self::assertEquals(0, $queue->numberOfItems());
    self::assertArrayHasKey('pokemon_species', $this->finishedPipelines);
    $finished_event = $this->finishedPipelines['pokemon_species'];
    self::assertEquals([
      'name' => 'Bulbasaur',
      'description' => "It carries a seed\non its back right\nfrom birth. As it\fgrows older, the\nseed also grows\nlarger.",
    ], $finished_event->getResult()->getValue());
  }

  /**
   * Event listener to track dispatched events.
   */
  public function trackFinishedPipelines(ProcessingFinishedEvent $event): void {
    $this->finishedPipelines[$event->getPipelineId()] = $event;
  }

}
